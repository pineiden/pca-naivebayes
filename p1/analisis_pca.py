from rich import print
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from source import Source
import pandas as pd
import matplotlib.pyplot as plt
import tkinter as tk
from tkinter import Tk, Frame
from tkinter import ttk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from functools import partial
from pathlib import Path
from tkinter import messagebox
from psynlig import pca_explained_variance_bar
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from frequency_gen import  FREQUENCIES
import numpy as np

class TkGui(Frame):
    def __init__(self, master):
        super().__init__(master)
        self.load_source()
        self.update_names()
        self.set_styles()
        master.bind('<Control-c>', master.quit)
        master.protocol("WM_DELETE_WINDOW", master.destroy)
        self.grid()        
        self.root = master
        self.root.title("Análisis PCA Serie Tiempo")
        self.save_widget()
        self.head_widget()
        self.checkboxes = {}
        self.actions = {}
        self.chart = None
        self.figure = None
        
        self.load_checkboxs(self.names)

        frame = ttk.Frame(self)
        frame['padding'] = (5, 10, 5, 10)
        self.load_freq_buttons(frame)
        self.load_channels_buttons(frame)
        self.enable_noise(frame)
        frame.grid(row=2, column=2, rowspan=20)
        self.run_pca()
        charts = Path(__file__).parent.parent / "charts"
        charts.mkdir(exist_ok=True, parents=True)

        
    def set_styles(self):
        style_frequency_button = ttk.Style()
        style_frequency_button.map(
            'C.TButton',
            foreground=[('pressed','red'),('active', 'blue')],
            background=[('pressed','!disabled','black'),('active','white')])
        
    def update_names(self):
        self.names = {f"PCA_{i}": True for i in range(self.source.n_channels)}
        
    def load_source(self):
        self.channels = 20
        data = {
            "n_times": 1000,
            "seconds": 10,
            "n_signals": 10,
            "n_channels": self.channels,
        }
        source = Source(**data)
        self.source = source
            
        
    def save_widget(self):
        frame = ttk.Frame(self)
        self.save_entry = ttk.Entry(frame)
        self.save_button = ttk.Button(
            frame,
            text="Guardar",
            command=self.save_chart)
        self.save_entry.pack(side=tk.LEFT)
        self.save_button.pack(side=tk.LEFT)
        self.params_var = tk.StringVar()
        self.params_var.set(self.source.parameters)
        ttk.Label(
            frame,
            textvariable=self.params_var).pack(side=tk.LEFT)                
        frame.grid(row=0, column=1)

    def head_widget(self):
        ttk.Button(self, text="Cerrar",
                   command=self.root.quit).grid(
                       row=0,
                       column=3)

        
    def save_chart(self):
        filename = self.save_entry.get()
        if not filename.endswith("png"):
            filename = f"{filename}.png"
        path = Path("charts") / filename
        self.figure.savefig(path)

    def load_freq_buttons(self, frame):
        i = 0
        def action_pca(key, operator):
            name, callback = operator.args
            self.source.set_path(key)
            freqs = self.source.create_freqs(name, callback)
            self.source.create_xy(freqs, self.source.time)
            self.source_plot(self.source)
            self.params_var.set(self.source.parameters)            
            self.run_pca()

        self.last = 2
        for key, operator in FREQUENCIES.items():
            ttk.Button(frame, text=str(operator),
                       command=partial(action_pca, key, operator),
                       style='C.TButton').grid(
                           row=i,
                           column=0,
                           columnspan=3,
                           sticky='nesw',
                           )
            i += 1

        self.last = i
        
    def load_channels_buttons(self, frame):
        # put a label.

        def update_source(n):
            #self.source.set_signals(n)
            self.source.set_signals(n)
            operator = FREQUENCIES.get("simple")
            name, callback = operator.args
            self.channels = self.source.n_channels
            freqs = self.source.create_freqs(name, callback)
            self.source.create_xy(freqs, self.source.time)
            self.update_names()
            self.source.set_path("simple")
            self.source_plot(self.source)
            self.params_var.set(self.source.parameters)            
            self.run_pca()

        tk.Label(frame, text="Number of signals-freqs").grid(row=self.last, column=0, columnspan=3, sticky='nesw')
        # generate numbers
        numbers = [i for i in range(2, 60, 3)]
        if self.source.n_signals not in numbers:
            numbers.append(self.source.n_signals)
            numbers.sort()            
        g = int(np.ceil(len(numbers)/3))
        grid = np.array_split(numbers, g)
        base_row = self.last + 1
        base_col = 0
        w = len(grid)
        c = len(grid[0])
        for i in range(w):
            for j in range(c):
                if i < len(grid) and j < len(grid[i]):
                    ttk.Button(frame, text=str(grid[i][j]),
                               command=partial(update_source, grid[i][j]),
                               style='C.TButton').grid(
                                   row=base_row+i,
                                   column=base_col+j,
                                   sticky='nesw')
        self.last = base_row + i + 1

    def enable_noise(self, frame):
        def update_names():           
            self.source.toggle_noise()
            operator = FREQUENCIES.get("simple")
            name, callback = operator.args
            self.channels = self.source.n_channels
            freqs = self.source.create_freqs(name, callback)
            self.source.create_xy(freqs, self.source.time)
            self.update_names()         
            self.source.set_path("simple")
            self.source_plot(self.source)            
            self.run_pca()
        
        var = tk.IntVar(value=0)        
        checkbox = tk.Checkbutton(
             frame,
             text="Habilitar ruido",
             onvalue=1,
             variable=var,
             command=update_names,
             offvalue=0)
        self.noise_checkbox = checkbox
        checkbox.grid(row=self.last, column=0, columnspan=3,sticky='nesw')

    def load_checkboxs(self, names):
        def update_names(i):
            key = f"PCA_{i}"
            self.names[key] = not self.names[key]
            self.update_chart()
            
        for i, name in enumerate(names):
            var = tk.IntVar(value=1)
            checkbox = tk.Checkbutton(
                self,
                text=name,
                onvalue=1,
                variable=var,
                command=partial(update_names,i),
                offvalue=0)
            checkbox.select()
            checkbox.grid(row=2+i, column=0)
            self.checkboxes[i] = checkbox

    def update_checkboxes(self):
        for i, ch in self.checkboxes.items():
            ch.select()

    def run_pca(self):
        source = self.source
        pca = PCA(n_components=source.n_channels)#source.n_signals)
        self.pca = pca        
        x = StandardScaler().fit_transform(source.channels)
        principalComponents = pca.fit_transform(x)
        df = pd.DataFrame(data=principalComponents, columns=self.names)
        pca_var = pca.explained_variance_ratio_
        self.counter = dict(zip(self.names, pca_var))
        print(pca.singular_values_)
        print(df.head())
        self.df = df
        self.update_checkboxes()
        self.source_plot(source)
        self.update_chart()

    def source_plot(self, source, gui=True):
        if self.chart:
            plt.close(self.chart)
        figure = source.plot()
        chart = FigureCanvasTkAgg(figure, self)
        chart.get_tk_widget().grid(row=1, column=0, columnspan=14)
        self.chart = figure
        
    def update_chart(self):
        if self.figure:
            plt.close(self.figure)
        df = self.df[[name for name, v in self.names.items() if v]]
        r,c = df.shape
        if r>=1 and c>=1:
            figure, (ax1, ax2) = plt.subplots(
                nrows=1, ncols=2,
                figsize=(14, 4),
                gridspec_kw={'width_ratios': [5, 3]})        
            ax1x = ax1.twinx()
            df.plot(ax=ax1x)
            box = ax1x.get_position()
            ax1x.set_position([box.x0, box.y0, box.width * 1.1, box.height])
            ax1x.legend(bbox_to_anchor=(1.2, 1.0), fontsize=8)

            plt.style.use('seaborn-talk')
            pca_explained_variance_bar(self.pca, axi=ax2, alpha=0.8)

            figure.tight_layout()

            # put on gui
            chart = FigureCanvasTkAgg(figure, self)
            chart.get_tk_widget().grid(row=2, column=1, rowspan=len(self.names))

            self.figure = figure
            path = self.source.path.replace("source", "pca")
            save_path = self.source.base / path
            self.figure.savefig(save_path)

        else:
            print("Dataset vacio")
def gui():
    try:
        root = Tk()
        print("TkGUI", TkGui)
        frm = TkGui(root)
        print("GUI->", frm)
        root.mainloop()
    except Exception as e:
        raise e
    except KeyboardInterrupt as ke:
        raise ke

if __name__ == '__main__':
    gui()
