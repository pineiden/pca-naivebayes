"""
"""
from dataclasses import dataclass
import matplotlib.pyplot as plt
import numpy as np
from rich import print
from frequency_gen import secuencial
from pathlib import Path

@dataclass
class Source:
    n_times: int
    seconds: int
    n_signals: int
    n_channels: int
    noise: bool =  False
    # crear acumulador de casos.

    def __post_init__(self):
        # array equiespaciado
        time = np.linspace(
            0,
            self.seconds,
            self.n_times)
        self.set_path("base")
        self.time = time 
        charts = Path(__file__).parent.parent / "charts"
        charts.mkdir(exist_ok=True, parents=True)
        self.base = charts

        # matriz zeros a cargar
        # generacion de frecuencias angulares
        # asociadas a cada canal
        freqs = self.create_freqs("básico")

        # carga con valores cada fila
        # a cada tiempo se multiplica frecuencia
        # se aplica la funcion cos
        self.create_xy(freqs, time)

    def create_freqs(self, name, callback=secuencial):
        self.name = name
        return callback(self.n_signals)

    def set_signals(self, n:int):
        self.n_signals = n

    def set_channels(self, n:int):
        self.n_channels = n

    def toggle_noise(self):
        self.noise = not self.noise

    def create_xy(self, freqs, time):
        X = np.zeros((self.n_times, self.n_signals))
        white_noise = np.zeros(len(time))
        for i in range(self.n_signals):            
            if self.noise:
                white_noise = np.random.normal(0, 1, len(time))
            X[:,i] = np.cos(freqs[i]* time) + white_noise

        # genera matriz de pesos NxM
        # asignará las amplitudes a cada canal
        mixing_weights = np.random.randn(
            self.n_signals, self.n_channels)
        
        # salida: mezcla las señales con pesos random
        Y = X @ mixing_weights
        self.X = X
        self.Y = Y
        
    def set_path(self, name):
        is_noisy = "_ruido" if self.noise else ""
        name = f"source_{name}_{self.n_signals}{is_noisy}.png"
        self.path = name

    @property
    def save_path(self):
        return self.base / self.path

    def plot(self, Y=[], name=""):
        if name != "":
            self.name = name
            is_noisy = "ruido" if self.noise else ""
            self.set_path(f"source_{name}_{self.n_signals}_{is_noisy}.png")
        fig, axs = plt.subplots(2, 1, figsize=(16,4),constrained_layout=True)
        
        axs[0].plot(self.time, self.X, label=f'X, series originales: {self.name}')
        axs[0].set_title(f'X, series originales: {self.name}')
        if not len(Y)>0:
            axs[1].plot(self.time, self.Y, label=f'series remezcladas: {self.name}')
            axs[1].set_title(f"series remezcladas: {self.name}")
        else:
            axs[1].plot(self.time, Y, label=f'series remezcladas: {self.name}')
            axs[1].set_title(f"series remezcladas: {self.name}")
        plt.savefig(self.save_path)
        return fig

    @property
    def channels(self):
        return self.Y#.T

    @property
    def parameters(self):
        return f"Signals {self.n_signals} | Freq. Gen. {self.name}"

if __name__ == '__main__':
    print("Análsis básico de fuente de señal")
    data = {
        "n_times": 1000,
        "seconds": 10,
        "n_signals": 10,
        "n_channels": 20,
    }
    print(data)
    source = Source(**data)
    source.plot()
