import math
import numpy as np

"""
Secuencial
"""

def secuencial(n):
    return  2 * np.pi * np.random.rand(n)


"""
frecuencias muy cercanas
"""

def near_freq(n):
    eps = 10e-8
    start=1
    freqs = []
    for i in range(n):
        freqs.append(start + i*eps)
    return 2 * np.pi * np.array(freqs)

"""
frecuencias pares
"""

def even_freqs(n):
    start = 2
    freqs = []
    for i in range(n):
        freqs.append(start + i*2)
    return 2 * np.pi * np.array(freqs)
    

"""
frecuencias impares
"""

def odd_freqs(n):
    start = 1
    freqs = []
    for i in range(n):
        freqs.append(start + i*2)
    return 2 * np.pi * np.array(freqs)


"""
frecuencias primos
"""

def nros_primos(n):
    primos = []
    inicio = 2
    base = n
    while len(primos) < base:
        for i in range(inicio, n):
            bandera = True
            for j in primos:
                if i%j == 0 and j > 1:
                    bandera = False
                    break
            if bandera:
                primos.append(i)
        inicio = primos[-1] if len(primos)>0 else 2
        n = inicio + base
        print(inicio, n, primos)
    freqs = primos[:base]
    return 2 * np.pi * np.array(freqs)

"""
exponenciales binarias
"""

def bin_freqs(n):
    freqs = []
    for i in range(n):
        freqs.append(2**i)
    return 2 * np.pi * np.array(freqs)


"""
exponenciales decimales
"""

def dec_freqs(n):
    freqs = []
    for i in range(n):
        freqs.append(10**i)
    return 2 * np.pi * np.array(freqs)


"""
crecimiento logaritmico
"""

def log_freqs(n):
    freqs = []
    for i in range(n):
        freqs.append(math.log(i+2))
    return 2 * np.pi * np.array(freqs)

from dataclasses import dataclass
from typing import Callable

@dataclass
class Operator:
    name:str
    callback: Callable

    def __str__(self):
        return self.name
    
    def __call__(self, value):
        return self.callback(value)

    @property
    def args(self):
        return self.name, self.callback

"""
En caso de crear una nueva función generadora de  serie, registrar aqui. 

"""
    
FREQUENCIES = {
    "simple": Operator("Frecuencias secuencial", secuencial),
    "near": Operator("Frecuencias cercanas", near_freq),
    "even": Operator("Frecuencias pares", even_freqs),
    "odd": Operator("Frecuencias impares", odd_freqs),
    "prime": Operator("Frecuencias nros primos", nros_primos),
    "bin": Operator("Frecuencias crecimiento exp binario", bin_freqs),
    "dec": Operator("Frecuencias crecimiento exp decimales", dec_freqs),
    "log": Operator("Frecuencias crecimiento logaritmico", log_freqs)
}
