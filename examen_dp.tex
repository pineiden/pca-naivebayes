% Created 2022-07-18 Mon 22:11
% Intended LaTeX compiler: pdflatex
\documentclass[letter,9pt,twoside,twocolumn,margin=.5in]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[utf8x]{inputenc}
\usepackage{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{upgreek}
\usepackage{etoolbox}
\AtBeginEnvironment{tabular}{\tiny}
\geometry{margin=1.0cm, top=1.0cm, headsep=1.0cm, footskip=1.0cm}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\author{David Pineda Osorio}
\date{\today}
\title{Aprendizaje de Máquinas: Examen}
\hypersetup{
 pdfauthor={David Pineda Osorio},
 pdftitle={Aprendizaje de Máquinas: Examen},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 28.1 (Org mode 9.5.2)}, 
 pdflang={Spanish}}
\begin{document}

\maketitle

\section{P1. PCA}
\label{sec:org1c7e6e5}

El método \emph{PCA} involucra realizar un escalado, centrado e
inclinar los ejes principales que definan cada dato. Una vez hecho
esto, se genera un nuevo dataset cuyos componentes permiten recrear el
dato original.

El caso de estudio consiste en N canales compuestos por M señales de
diferentes frecuencias. Se puede hacer una analogía a una fuente, como
un sensor, y diferentes medidas que ocurran al mismo tiempo. Se simula
la generación de estos valores mediante el uso de la siguiente función
como base.

$$
x(t) = a_i  cos(2 \pi f_i t)
$$

En que:

\(a_i\) es la amplitud

\(f_i\) es la frecuencia.

Una representación de esto se puede ver en la figura \ref{chart:1}, en que
se establecen señales a diferentes frecuencias y se componen mediante
una remezcla.  Los canales serían las \emph{variables} y el resultado del
método \emph{PCA} consiste en un conjunto de \emph{componentes}, series de tiempo y sus
coeficientes, los cuales se seleccionan los de mayor incidencia.  

\begin{figure}[htbp]
\centering
\includegraphics[width=220px]{charts/source_base_10.png}
\caption{\label{chart:1}Señales y canales para una fuente simulada}
\end{figure}


El estudio de de este caso permitirá establecer el
efecto de los principales parámetros generadores de los canales,
incluida la adición del ruido.

Se crea una interfaz gráfica (\textbf{analisis\_pca.py}) que permita ir ajustando los diferentes
parámetros solicitados, Al utilizar la \emph{GUI} 
podrás interactuar, visualizando las principales 
gráficas para interpretar el análisis \emph{PCA}. 

\begin{figure}[htbp]
\centering
\includegraphics[width=250px]{p1/gui.png}
\caption{\label{fig:gui}Interfaz gráfica análisis PCA}
\end{figure}


\subsection{Efecto de las frecuencias.}
\label{sec:orgc2049d6}

Se estudia el efecto de diferentes series de frecuencias originales, para eso se
crea para cada tipo de serie una función que crea el valor base con
que se inicializan. Este primer análisis será con 10 señales de frecuencia.

En el archivo \textbf{frequency\_gen.py} se pueden registrar diferentes
generadoras de frecuencia, contando con las básicas y de interés dado
que abordan diferentes casos frecuentes para series de valores.

\begin{center}
\begin{tabular}{ll}
función & descripción\\
\hline
secuencial & serie aleatoria\\
near\_freq & frecuencias muy cercanas\\
even\_freqs & serie pares\\
odd\_freqs & serie impares\\
nros\_primos & números primos\\
bin\_freqs & valores 2\^{}n\\
dec\_freqs & valores 10\^{}n\\
log\_freqs & valores log(n)\\
\end{tabular}
\end{center}

Se observa que el efecto de una serie de frecuencias específicas
dependerá de la cercanía de su valor entre ellas\ref{chart:2}. Por ejemplo, para
frecuencias muy cercanas se tiene que la componente principal será
prácticamente una. 

\begin{figure}[htbp]
\centering
\includegraphics[width=240px]{charts/pca_near_10.png}
\caption{\label{chart:2}PCA para señales con frecuencias muy cercanas}
\end{figure}

Las combinaciones de series \emph{pares} e \emph{impares} tienen un resultado
muy similar para el análisis PCA, observándose que hasta la décima
componente \emph{PCA\_10} se tienen un resultado satisfactorio, para 10
señales diferentes. Esto es porque se distinguen con claridad cada valor,
existiendo relaciones de ortogonalidad entre funciones coseno con frecuencias
linealmente espaciadas.

\begin{figure}[htbp]
\centering
\includegraphics[width=240px]{charts/pca_even_10.png}
\caption{\label{chart:3}PCA para series pares}
\end{figure}

Cuando se observa para las series aleatorias que las componentes
principales son menores que la cantidad de señales entregadas se puede
deducir que hay valores con frecuencias  muy cercanas, lo que es
lógico dado que no otorga información extra.

\begin{figure}[htbp]
\centering
\includegraphics[width=240px]{charts/secuencia_base_10.png}
\caption{\label{chart:4}PCA para series aleatorias}
\end{figure}

Se refuerza la observación cuando nos enfrentamos a secuencia de
números primos, en que se puede decir que disminuye la relación entre
cada uno de ellos. El análisis \emph{PCA} muestra la necesidad de las 10
componentes esperadas, tal como en los casos pares e impares, las componentes
relevantes son cosenos ortogonales.

\begin{figure}[htbp]
\centering
\includegraphics[width=240px]{charts/pca_prime_10.png}
\caption{\label{chart:5}PCA para series primos}
\end{figure}

También es interesante observar que la base numérica de operación es
\emph{decimal}, cuando se genera una serie de frecuencias exponenciales de
10 (10\^{}n) el análisis \emph{PCA}. Las componentes principales se reducirán
a 3. 

\begin{figure}[htbp]
\centering
\includegraphics[width=240px]{charts/pca_dec_10.png}
\caption{\label{chart:6}PCA para exponenciales de 10}
\end{figure}

Para el análisis en series binarias y logarítmica nos encontramos en
valores lo suficientemente diferentes como para necesitar las 10 PCA.

En conclusión, dada una serie de frecuencias, la cantidad de
componentes principales dependerá tanto del ordinal de ella, como de
la relación entre los valores, en cuanto a la información que se puede
desprender una de otra.

Para este tipo de análisis se deben considerar conocimientos de análisis de señales, en que existe el 
concepto de \emph{frecuencia nyquist} em que, para series discretas, no pueden
contener frecuencias superiores a \(f_n = f_s/2\), la frecuencia de muestreo. Por
lo que aquellas señales que superan esta frecuencia parecerán no existir, pero
si están. Una forma de resolver este problema será aumentar la tasa de muestreo.   

En un análisis \emph{PCA} se deberá, por lo tanto, tomar la decisión sobre el grado
de importancia de las señales no consideradas por este efecto. Si son de
importancia, reajustar la muestra o mantener la solución.

\subsection{Análisis por cantidad de señales.}
\label{sec:org62006d9}

Fijando la serie de frecuencias en \emph{primos}, para asegurarnos 'ceteris
paribus',  evaluamos que pasa al variar la cantidad de señales a
generar.

Para series de secuencias crecientes y suficientemente
diferentes, que la cantidad de componentes principales del análisis
\emph{PCA} presentan una tendencia a converger a un valor cercano a la
cantidad de canales. 

\begin{figure}[htbp]
\centering
\includegraphics[width=240px]{charts/pca_prime_59.png}
\caption{\label{chart:7}PCA convergiendo a cantidad de canales}
\end{figure}

La curva \ref{chart:8} que describe este comportamiento.

\begin{figure}[htbp]
\centering
\includegraphics[width=220px]{p1/aumento_signales_pca.png}
\caption{\label{chart:8}Curva convergencia cantidad componentes}
\end{figure}

Utilizando la interfaz, se observa un comportamiento similar para las
diferentes generadoras de frecuencias. Para series de valores que son
completamente diferentes, la convergencia ocurrirá a partir de la
cantidad de canales, para la serie primos la curva es prácticamente
igual a la cantidad de señales hasta que supera la cantidad de canales.

Una conclusión interesante parece ser que la curva se define de tal
manera que su valor máximo será para el caso en que todas las
frecuencias sean diferentes y cada una de ellas provea la suficiente
información distinta de las otra, como parece ser el caso de la serie
de números primos.

\subsection{Estudio de señales con ruido.}
\label{sec:orgfc65f34}

Considerando un \emph{ruido blanco} que se añade a la señal, se habilita. En
la interfaz basta con aplicar al \emph{checkbox} 'Habilitar ruido' para
observar el efecto.

Se observa directamente en \ref{chart:9} que el efecto es añadir incertidumbre a la
información de cada señal, por lo que el análisis \emph{PCA} repercute en
un aumento de la cantidad de componentes principales con el fin de
regular este efecto. Este factor dependerá de la amplitud y frecuencia.

\begin{figure}[htbp]
\centering
\includegraphics[width=180px]{p1/ruido/efecto_ruido.png}
\caption{\label{chart:9}Efecto del ruido en análisis PCA}
\end{figure}

Si se utiliza la \emph{gui}, se pueden probar distintos casos con el ruido
activado. En cada caso el ruido aporta la incertidumbre observada.

\newpage

\section{P2. Naive Bayes}
\label{sec:org0594244}

El método \emph{Naive Bayes} consiste en aplicar la \emph{teoría de Bayes} en la
modelación de un sistema bajo algunos supuestos de importancia.

Uno de los \emph{supuestos}, que dan nombre al método es que los eventos considerados en el
estudio de un 'fenómeno' en que las componentes son independientes
entre sí, una idea \emph{ingenua}.

Además, para utilizar la inferencia bayesiana se debe considerar que
el sistema es estoćastico, lo que permitirá aplicar las reglas
bayesianas. Permitiendo decidir en base a la información disponible.

Es decir, dada una ocurrencia de B, podríamos saber la probabilidad de que
ocurra A, en la forma bayesiana:

$$
P(A | B) = \frac {P (A)  P(B | A)} {P(B)}
$$

Dado esto, se pueden aplicar en algoritmos de clasificación o de
aprendizaje de máquinas. Permite construir un modelo en base al
cálculo de las \emph{probabilidad posterior}  de que ocurra un evento A,
dado que haya ocurrido un evento B.

En términos generales, es un modelo basado en una estimación de la siguiente probabilidad.

$$
P(A | B_1,B_2,B_3,..,B_n)
$$

Para iniciar la comprensión de este método se tiene el siguiente ejemplo.

En una línea de empaquetado (maquiladora)  por la que pasa fruta y es
necesario clasificar y seleccionar.

\begin{itemize}
\item manzanas, de piel \textbf{roja} o \textbf{verde}
\item peras, de piel \textbf{verde} o \textbf{roja} (muy pocas)
\end{itemize}

Se tiene un registro de que en diez mil unidades hay un un 40\% de peras  y un
60\% de manzanas.

Podemos inferir, ya que es un número suficientemente grande, las probabilidades
totales para cada caso.

\begin{center}
\begin{tabular}{lr}
F:=fruta & prior:=P(F)\\
\hline
manzana & .6\\
pera & .4\\
\end{tabular}
\end{center}

De entre las manzanas un 20\% son de piel verde y el resto rojas. De entre las
peras solo un 1\% presenta tonos rojos en su piel, lo que podría confundir a una
\emph{maquina} entrenada.

Si ponemos esta información en una tabla. Nos permite calcular el 
correspondiente, que es al probabilidad para ese  caso considerando los casos totales.

\begin{center}
\begin{tabular}{llrrr}
F:=fruta & C:=color & P(F) & verosimilitud:=P(C\vertF) & P(F)*P(C\vertF)\\
\hline
manzana & rojo & .6 & .8 & 0.48\\
manzana & verde & .6 & .2 & 0.12\\
pera & rojo & .4 & .01 & 4e-3\\
pera & verde & .4 & .99 & 0.396\\
\end{tabular}
\end{center}

Con esto, nos interesaría saber que pasa al \emph{observar} la línea y tener una
estimación de la 'próxima' fruta correctamente, ¿será 'roja' o 'verde'? Luego de
determinar el color, ¿será pera o manzana?

Es decir, nos interesa calcular la \textbf{posterior} correspondiente a cada caso.

Para que la fruta sea \emph{manzana} dado que el color es rojo.

Abreviando con:

\begin{description}
\item[{m}] manzana
\item[{p}] pera
\item[{r}] rojo
\item[{v}] verde
\end{description}

$$
P(m | r) = \frac {P(m)*P(r|m)} {P(p)*P(r|p) +P(m)*P(r|m)}
$$

Para que la fruta sea \emph{manzana} dado que el color es verde.

$$
P(m | v) = \frac {P(m)*P(v|m)} {P(p)*P(v|p) +P(m)*P(v|m)}
$$

Para que la fruta sea \emph{pera} dado que el color es verde.

$$
P(p | r) = \frac {P(p)*P(r|p)} {P(p)*P(r|p) +P(p)*P(r|p)}
$$

Para que la fruta sea \emph{pera} dado que el color es verde.

$$
P(p | v) = \frac {P(p)*P(v|p)} {P(p)*P(v|p) +P(p)*P(v|p)}
$$

Tendremos la tabla.

\begin{center}
\begin{tabular}{llr}
Posterior & Valor & total\\
\hline
P(m \(\vert{}\) rojo) & 0.48/(0.48+4e-3) & 0.99173554\\
P(m \(\vert{}\) verde) & 0.12/(0.12+0.396) & 0.23255814\\
P(p \(\vert{}\) rojo) & 4e-3/(0.48+4e-3) & 8.2644628e-3\\
P(p \(\vert{}\) verde) & 0.396/(0.12+0.396) & 0.76744186\\
\end{tabular}
\end{center}

Como era de esperar, para los casos en que la piel sea roja es muy poco probable
que la fruta catalogada sea \emph{pera}. Sin embargo, dado que el \emph{verde} es un atributo
fuerte en la \emph{pera} y relativamente común en la manzana habrá un factor de error
al clasificar si solo tomamos en cuenta este valor.

En general, para casos con más características, se deberá
considerar la verosimilitud (multiplicación de las probabilidades) de los
parámetros que comprendan la definición del objeto, como \(\{color,forma,
peso\}\)  que será el conjunto que defina si la fruta es o no manzana o pera.

En un modelo clasificador se estima la \emph{posterior} y se evalua el valor máximo
para definir la clase (o fruta) determinada, obedeciendo a la regla de decisión.
Siendo F para frutas, y X para las características.

$$
clase(x_1,..,x_n) = \arg \max_{f} {p(F=f) \Pi_{i=1}^n p(X_i=x_i | F=f)}
$$

Con esto en cuenta, para el caso de las peras y manzanas, en que se considere
solo el color de la piel, terminará por clasificar las rojas como manzanas y las
verdes como pera, lo que no siempre será cierto. En esto se evidencia una de las
debilidades de este método.

También,  si en algún momento pasa una fruta con piel de color
\emph{amarillo}. Que puede ser tanto manzana (verde madura) o pera con ese tono. El
clasificador no podrá hacer la predicción, ya que no hay registro para ese caso.

Si consideramos como al conjunto \emph{tipo de fruta} como clases, y sus \emph{atributos}
a las características que la definan. Para una cierta serie de valores
definidos (considerando dos atributos: color, forma: redonda, ovalada), como:

$$
X = \{rojo, redonda\} 
$$

La probabilidad \emph{posterior} de que la fruta sea \emph{manzana} será:

$$
P(m |  r \cap red.) = \frac {P(r \cap red. | m) P(m)} {P(r \cap red.)}
$$

Sabiendo que cierta parte de las \emph{peras} son rojas y pocas, también, podrían
ser \emph{redondas}. De esta forma será posible refinar la clasificación mediante el
uso de la teoría bayesiana.

Este método es una herramienta que se puede aplicar con facilidad para
clasificación binaria o multiclase.

En términos computacionales requiere:

1.- Convertir el conjunto de datos a frecuencia de sus ocurrencias (histograma).

2.- Crear una tabla de probabilidades para cada evento.

3.- Calcular la probabilidad posterior por clase.

4.- Aquella clase que resulte tener una probabilidad más alta es el
resultado de la predicicón.

Con esto tenemos a \emph{Naive Bayes}, un método sencillo que aprovecha las
características bayesianas de un fenómeno para realizar clasificación, bajo el
supuesto (fuerte) que las características que definan la clase son independientes.

Cuando el modelo contiene un atributo continuo, por cada clase se estiman los
valores de media \(\mu\) y varianza \(\sigma^2\), permitiendo definir como otra
hipótesis que esta característica lleva una distribución normal \(\mathcal{N}(\mu, \sigma)\).


\newpage

\section{Anexo}
\label{sec:org64c896c}

Código fuente, acceso a la \emph{GUI} y archivos originales.

\begin{description}
\item[{repo}] \url{https://gitlab.com/pineiden/pca-naivebayes}
\end{description}
\end{document}